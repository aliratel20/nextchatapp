import React, { FormEventHandler, InputHTMLAttributes, ReactEventHandler, ReactNode, useRef, useState } from "react";
import { UseFormRegisterReturn } from "react-hook-form";

interface ResizableTextareaProps {
  maxRows: number;
  className?:string;
}

export const ResizableTextarea:React.FC<ResizableTextareaProps> = ({maxRows,className} ) => {
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const [value, setValue] = useState('');
  const [rows, setRows] = useState(1);

  const handleChange:React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
    const textarea = textareaRef.current;
    if(!textarea) return
    setValue(textarea.value);

    const computedStyle = window.getComputedStyle(textarea);
    const lineHeight = parseInt(computedStyle.lineHeight);

    const previousRows = textarea.rows;
    textarea.rows = 1; // Resetting rows to recalculate rows properly
    const currentRows = Math.ceil(textarea.scrollHeight / lineHeight - 1);

    if (currentRows <= maxRows) {
      textarea.rows = currentRows;
      setRows(currentRows);
    } else {
      textarea.rows = maxRows;
      textarea.scrollTop = textarea.scrollHeight;
    }
  };

  return (
    <textarea
      ref={textareaRef}
      value={value}
      onChange={handleChange}
      rows={rows}
      className={`resize-none w-full overflow-y-auto ${className ? className : ''}`}
      style={{ resize: 'none', width: '100%', overflowY: 'auto' }}
    />
  );
};


interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  type?: string;
  className?: string;
  placeholder: string;
  register?: UseFormRegisterReturn;
}

export function FORM({
  children,
  className,
  onSubmit,
}: {
  children: ReactNode;
  className?: string;
  onSubmit?: FormEventHandler<HTMLFormElement>;
}) {
  return (
    <form
      onSubmit={onSubmit}
          className={`${className ? className : ``} w-[390px] relative mx-auto flex flex-col items-center space-y-4 p-14  bg-gran shadow-2xl transition-shadow duration-1000 rounded-t-3xl rounded-br-3xl h-[400px]`}
    >
      {React.Children.map(children, (child) => {
        // You can modify the child element here if needed
        return child;
      })}
    </form>
  );
}

export const Input = ({
  type,
  className,
  placeholder,
  register,
}: InputProps) => {
  return (
    <input
      type={type}
      placeholder={placeholder}
      className={`p-2 w-[260px] focus:outline-none bg-white shadow-md rounded-md ${
        className ? className : ""
      }`}
      {...register} // Spread the register props here
      accept="image/*"
    />
  );
};

export const FileInput = ({
  className,
  register,
}: {
  className: string;
  register?: UseFormRegisterReturn;
}) => {
  return (
    <input
      type="file"
      className={`shadow-md rounded-full ${className ? className : ""}`}
      {...register} // Spread the register props here
      accept="image/*"
    />
  );
};

export const Button = ({
  text,
  onClick,
  onSubmit,
}: {
  text: string;
  onClick?: FormEventHandler<HTMLButtonElement>;
  onSubmit?: FormEventHandler<HTMLButtonElement>;
}) => {
  return (
    <button
      onSubmit={onSubmit}
      onClick={onClick}
      className="px-6 py-2 bg-white rounded-full shadow-lg font-bold font-mono"
    >
      {text}
    </button>
  );
};
