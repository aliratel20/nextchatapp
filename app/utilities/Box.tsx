import React, { ReactNode } from "react";

export function AuthBox({ children, header }: { children: ReactNode; header:string }) {
  return (
    <div className="">
      <div className="space-y-4">
      <h1 className="text-6xl text-center my-2 font-bold">{header}</h1>
        {React.Children.map(children, (child) => {
          // You can modify the child element here if needed
          return child;
        })}
      </div>
    </div>
  );
}
