import Link from "next/link";

export default function Navbar() {
    return (
        <nav className="flex justify-around items-center p-2 rounded-3xl shadow-xl bg-gran mx-2 mt-4">
            <ul>
                <li className="font-bold text-xl">NextChat</li>
            </ul>            
            <ul className="flex gap-4">
                <li><Link href={'/home/chat'}>Chat</Link></li>
                <li><Link href={'/home/setting'}>Setting</Link></li>
                <li>Logout</li>
            </ul>
        </nav>
    )
}