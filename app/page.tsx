"use client";
import { setUser } from "@/lib/features/user/userSlices";
import { auth } from "@/lib/firebase";
import { redirect } from "next/navigation";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

export default function Home() {
    const dispatch = useDispatch() 
    useEffect(() => {
        auth.onAuthStateChanged((data) => {
            console.log(data)
          dispatch(setUser(data?.email));
        });
      },[]);
  return redirect("/home");
}
