'use client'
import { ResizableTextarea } from "@/app/utilities/Form"
import { setUser } from "@/lib/features/user/userSlices"
import { auth } from "@/lib/firebase"
import { RootState } from "@/lib/store"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"

export default function page() {
  const user = useSelector((state:RootState) => state.user)
  return (
    <div className="absolute p-3 mt-2 right-4 w-3/4 h-[85vh] shadow-2xl rounded-r-3xl ">
        <div className="relative text-black bg-[#f7f0d4] w-full h-full rounded-r-3xl rounded-b-3xl">
          <div></div>
          <div className="flex absolute bottom-0 w-full">
          <ResizableTextarea className="rounded-b-3xl w-full py-2 px-3" maxRows={4} />
          </div>
        </div>
    </div>
  )
}
