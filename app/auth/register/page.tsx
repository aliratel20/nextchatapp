"use client";
import Link from "next/link";
import { Input, Button, FORM } from "../../utilities/Form";
import { useEffect } from "react";
import { auth, app } from "@/lib/firebase";
import { z } from "zod";
import { RegisterSchema } from "./zod/schema";
import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import {
  createUserWithEmailAndPassword,
  sendEmailVerification,
} from "firebase/auth";
import { AuthBox } from "@/app/utilities/Box";
import { useRouter } from "next/navigation";

export default function page() {
  type fromfileds = z.infer<typeof RegisterSchema>;
  const router = useRouter()
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<fromfileds>({
    resolver: zodResolver(RegisterSchema)
  });

  const Submit: SubmitHandler<fromfileds> = async (data) => {
    try {
      await createUserWithEmailAndPassword(
        auth,
        data.email,
        data.password
      )
      await alert("sign up is susccefull");
      await auth.signOut()
      await router.push('/auth/login')
      console.log(data);
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
        setError("root", {
          message: "this register invalid",
        });
      }
    }
  };
  useEffect(() => {
    auth.onAuthStateChanged((data) => {
      console.log(data?.emailVerified);
    });
  }, []);
  return (
    <AuthBox header="Register">
        <FORM onSubmit={handleSubmit(Submit)}>
        <div >
          <Input type="text" placeholder="Email" register={register("email")} />
          {errors.email && (
            <p className="w-[100px] text-xs my-1 text-red-500 font-normal">
              {errors.email.message}
            </p>
          )}
        </div>
        <div >
          <Input
            type="password"
            placeholder="Password"
            register={register("password")}
          />
          {errors.password && (
            <p className="w-[180px] text-xs my-1 text-red-500 font-normal">
              {errors.password.message}
            </p>
          )}
        </div>
        <div>
          <Input
            type="password"
            placeholder="Confirm Password"
            register={register("confirmPassword")}
          />
          {errors.confirmPassword && (
            <p className="text-xs my-1 text-red-500 font-normal">
              {errors.confirmPassword.message}
            </p>
          )}
        </div>
        <div className="absolute bottom-12">
          <Button text="Create Account" />
        </div>
      <span className="absolute bottom-1">
        Already have an account ? {" "}
        <Link className="font-bold" href="/auth/login">
          Login
        </Link>
      </span>
        </FORM>
    </AuthBox>
  );
}

/**
 * <div >
            <Input
              type="text"
              placeholder="UserName"
              register={register("username")}
            />
            {errors.username && (
              <p className="text-xs my-1 text-red-500 font-normal">
                {errors.username.message}
              </p>
            )}
          </div>
          <div >
            <p>upload you image</p>
            <div className="flex my-2 justify-center">
              <div>
                <FileInput
                  className="z-10 absolute w-20 h-20 rounded-full opacity-0 cursor-pointer"
                  register={register("file")}
                />
                <Image
                  className="relative w-20 h-20 rounded-full"
                  src={pic}
                  alt="picture"
                />
              </div>
            </div>
            {errors.file && typeof errors.file.message === "string" && (
              <p className="block text-xs text-center my-1 text-red-500 font-normal">
                {errors.file.message}
              </p>
            )}
          </div>
 */
