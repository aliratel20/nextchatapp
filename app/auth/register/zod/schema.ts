import { z } from "zod";


export const RegisterSchema = z.object({
  email: z.string().email(),
  password: z.string().min(8),
  confirmPassword: z.string()
})
  .refine((data) => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });

const MAX_FILE_SIZE = 1000000;
const ACCEPTED_IMAGE_TYPES = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/webp",
];


export const CompleteProfile = z.object({
  username: z.string().min(2).max(10),
  file: z
    .any()
    // To not allow empty files
    .refine((files) => files?.length >= 1, {
      message: "Image is required.",
    })
    // To not allow files other than images
    .refine((files) => ACCEPTED_IMAGE_TYPES.includes(files?.[0]?.type), {
      message: ".jpg, .jpeg, .png and .webp files are accepted.",
    })
    // To not allow files larger than 5MB
    .refine((files) => files?.[0]?.size <= MAX_FILE_SIZE, {
      message: `Max file size is 1MB.`,
    }),
})


/**
 * const MAX_FILE_SIZE = 1000000;
const ACCEPTED_IMAGE_TYPES = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/webp",
];
 * export const RegisterSchema = z.object({
    username: z.string().min(2).max(10),
    email: z.string().email(),
    password: z.string().min(8),
    confirmPassword: z.string(),
    file: z
      .any()
      // To not allow empty files
      .refine((files) => files?.length >= 1, {
        message: "Image is required.",
      })
      // To not allow files other than images
      .refine((files) => ACCEPTED_IMAGE_TYPES.includes(files?.[0]?.type), {
        message: ".jpg, .jpeg, .png and .webp files are accepted.",
      })
      // To not allow files larger than 5MB
      .refine((files) => files?.[0]?.size <= MAX_FILE_SIZE, {
        message: `Max file size is 1MB.`,
      }),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: "Passwords don't match",
    path: ["confirmPassword"],
  });
 * 
 * 
 */