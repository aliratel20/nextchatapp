import { AuthBox } from "@/app/utilities/Box";
import { Button, FORM, Input } from "@/app/utilities/Form";
import Link from "next/link";

export default function ForgotPwd() {
  return (
    <AuthBox header="Reset Password">
        <FORM>
          <div>
            <Input
              type="password"
              placeholder="New Passowrd"
            />
            {/* {errors.email && (
              <p className="text-xs my-1 font-normal text-red-500">{errors.email.message}</p>
            )} */}
          </div>
          <div>
            <Input
              type="password"
              placeholder="Confirm Password"
            />
            {/* {errors.password && (
              <p className="text-xs my-1 font-normal text-red-500">{errors.password.message}</p>
            )} */}
          </div>
          <div className="absolute bottom-12 text-center">
            <Button text="Apply" />
          </div>
          <span className="absolute bottom-1">
            {/* You have'nt an Account ? <Link className="font-bold" href="/auth/register"> Register</Link> */}
          </span>
        </FORM>
      </AuthBox>
  )
}

