'use client'
import { AuthBox } from "@/app/utilities/Box";
import { Button, FORM } from "@/app/utilities/Form";
import { RootState } from "@/lib/store";
import firebase from "firebase/compat/app";
import { useSelector } from "react-redux";

export default function page() {
  const user = useSelector((state: RootState) => state.user)
  const handleResendVerification = async () => {
    try {
      const user = firebase.auth().currentUser;
      if (!user) {
        throw new Error("Not logged in");
      }

      if (user.emailVerified) {
        throw new Error("Email already verified");
      }

      await user.sendEmailVerification();
      return { success: true, message: "Verification email sent" };
    } catch (error) {
      if (error instanceof Error) {
        return { success: false, message: error.message };
      }
    }
  };
  return (
    <AuthBox header={`mr. ${user.email} Welcome to NextChat!`}>
      <FORM>
        <h2 className="text-center font-normal text-xl">
          Please check your email to verify your Account,{" "}
          <i>check spam folders</i>
        </h2>
        <p className="absolute bottom-16">if the email did not send, please</p>
        <div className="absolute bottom-4">
          <Button text="reSend Email Verify" />
        </div>
      </FORM>
    </AuthBox>
  );
}
