"use client";

import Link from "next/link";
import { Input, Button, FORM } from "../../utilities/Form";

import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { z } from "zod";
import { AuthBox } from "@/app/utilities/Box";
import { schema } from "./zod/schema";
import { auth } from "@/lib/firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import { useRouter } from "next/navigation";
import { setUser } from "@/lib/features/user/userSlices";
import { useDispatch } from "react-redux";


type fromfileds = z.infer<typeof schema>;

export default function page() {
  const router = useRouter()
  const dispatch = useDispatch()
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<fromfileds>({
    resolver: zodResolver(schema),
  });
  
  const Submit: SubmitHandler<fromfileds> = async (data) => {
    try {
      await signInWithEmailAndPassword(auth,data.email,data.password)
      await alert("login success")
      dispatch(setUser(data))
      await router.push('/auth/verify-email')
    } catch (error) {
      alert("error");
      setError("root", {
        message: "this login invalid",
      });
    }
  };

  return (
    <AuthBox header="Login">
        <FORM
          onSubmit={handleSubmit(Submit)}
        >
          <div>
            <Input
              type="text"
              placeholder="Email"
              register={register("email")}
            />
            {errors.email && (
              <p className="text-xs my-1 font-normal text-red-500">{errors.email.message}</p>
            )}
          </div>
          <div>
            <Input
              type="password"
              placeholder="Password"
              register={register("password")}
            />
            {errors.password && (
              <p className="text-xs my-1 font-normal text-red-500">{errors.password.message}</p>
            )}
          </div>
          <div className="flex justify-start">
            <Link className="" href={'/auth/reset-password'}>Forgot password ?</Link>
          </div>
          <div className="absolute bottom-12  text-center">
            <Button text="Login" />
          </div>
          <span className="absolute bottom-1">
            You have'nt an Account ? <Link className="font-bold" href="/auth/register"> Register</Link>
          </span>
        </FORM>
      </AuthBox>
  );
}
