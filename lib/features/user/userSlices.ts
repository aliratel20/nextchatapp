  import { createSlice } from "@reduxjs/toolkit";
  interface UserState {
    email?: string;
  }
  const initialState: UserState = {
    email:'no user'
  };
  export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
      setUser(state,action) {
          state.email = action.payload;
      },
      Logout(state) {
          state.email = undefined;
      }
    },
  });

  export const {setUser,Logout} = userSlice.actions;
  export default userSlice;