
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDwuun2nv1hhbdEfV8l72j11hJ2SUZDZec",
  authDomain: "nextchatweb.firebaseapp.com",
  projectId: "nextchatweb",
  storageBucket: "nextchatweb.appspot.com",
  messagingSenderId: "297598093191",
  appId: "1:297598093191:web:4a544129ef6d35d7bcb522",
  measurementId: "G-BT95EG3P7D"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth(app)

export { app, auth }